package joel.pirmeraaplicacio2021_dam2;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

import joel.pirmeraaplicacio2021_dam2.PomaXML;

@Root(name = "dades")
public class dadesXML {

    @ElementList(required = true, inline = true)
    private ArrayList<PomaXML> pomes;

    public dadesXML(){
        this.pomes = new ArrayList<>();
    }

    public ArrayList<PomaXML> getPomes(){
        return pomes;
    }

}
