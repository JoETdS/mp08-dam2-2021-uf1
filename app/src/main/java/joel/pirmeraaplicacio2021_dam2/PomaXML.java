package joel.pirmeraaplicacio2021_dam2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "poma")
public class PomaXML {

    @Element(name = "nom")
    private String nom;
    @Element(name = "pes")
    private int pes;
    @Element(name = "color")
    private String color;

    public PomaXML(String nom, int pes, String color) {
        this.nom = nom;
        this.pes = pes;
        this.color = color;
    }

    public PomaXML(){

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPes() {
        return pes;
    }

    public void setPes(int pes) {
        this.pes = pes;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
