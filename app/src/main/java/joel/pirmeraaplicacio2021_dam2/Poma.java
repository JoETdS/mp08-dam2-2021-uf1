package joel.pirmeraaplicacio2021_dam2;

public class Poma {

    private String nom;
    private int pes;
    private String color;

    public Poma(String nom, int pes, String color) {
        this.nom = nom;
        this.pes = pes;
        this.color = color;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPes() {
        return pes;
    }

    public void setPes(int pes) {
        this.pes = pes;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
