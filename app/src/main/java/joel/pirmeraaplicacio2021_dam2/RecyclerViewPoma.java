package joel.pirmeraaplicacio2021_dam2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewPoma extends RecyclerView.Adapter<RecyclerViewPoma.ViewHolder> {

private final List<PomaXML> llistaPomes;
private final FirstFragment.OnListFragmentInteractionListener mListener;

public RecyclerViewPoma(List<PomaXML> items, FirstFragment.OnListFragmentInteractionListener listener) {
        llistaPomes = items;
        mListener = listener;
        }

@Override
public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.llista_pomes, parent, false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.poma = llistaPomes.get(position);
        holder.hNomPoma.setText(llistaPomes.get(position).getNom());
        holder.hPesPoma.setText(String.valueOf(llistaPomes.get(position).getPes()));
        holder.hColorPoma.setText(llistaPomes.get(position).getColor());

        holder.mView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        if (null != mListener) {
        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mListener.onListFragmentInteraction(holder.poma);
        }
        }
        });
        }

@Override
public int getItemCount() {
        return llistaPomes.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final TextView hNomPoma;
    public final TextView hPesPoma;
    public final TextView hColorPoma;
    public PomaXML poma;

    public ViewHolder(View view) {
        super(view);
        mView = view;
        hNomPoma = (TextView) view.findViewById(R.id.poma_nom);
        hPesPoma = (TextView) view.findViewById(R.id.poma_pes);
        hColorPoma = (TextView) view.findViewById(R.id.poma_color);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + hPesPoma.getText() + "'";
    }
}
}
