package joel.pirmeraaplicacio2021_dam2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class FirstFragment extends Fragment {

    private Bundle b;

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;
    private List<PomaXML> pomes = new ArrayList<PomaXML>();
    private RecyclerViewPoma mAdapter;

    private TextView textview_first;

    private SQLiteDatabase baseDades;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        b = getArguments();
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        // Set the adapter
        if (view.findViewById(R.id.llista_pomes) instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.llista_pomes);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            this.mAdapter = new RecyclerViewPoma(this.pomes, mListener);
            recyclerView.setAdapter(this.mAdapter);
        }

        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //String user = getActivity().getIntent().getExtras().getString("user");

        this.baseDades = getActivity().openOrCreateDatabase("user", MODE_PRIVATE, null);
        String query = "Select * from usuaris";
        Cursor resultat = this.baseDades.rawQuery(query, null);
        resultat.moveToLast();
        String user = resultat.getString(1);

        //SharedPreferences dadesUsuari = getActivity().getSharedPreferences("preferencies", Context.MODE_PRIVATE);
        //String user = dadesUsuari.getString("user", "Alumne");

        TextView tv = view.findViewById(R.id.textview_first);
        if(b != null) {
            tv.setText(getString(R.string.comingFrom) + b.getString("n_fragment"));
        }else{
            tv.setText("hola " + user );
        }
        b = new Bundle();
        b.putString("n_fragment", getString(R.string.toFirst));
        view.findViewById(R.id.button_toSecond).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment, b);
            }
        });
        view.findViewById(R.id.button_toThird).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_ThirdFragment, b);
            }
        });
        view.findViewById(R.id.button_toCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_CameraFragment);
            }
        });
        getActivity().findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_AudioFragment);
            }
        });

        this.textview_first = (TextView) view.findViewById(R.id.textview_first);
        llegirXML();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //afegirPomes();
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    private void llegirXML() {

        AssetManager am = getActivity().getAssets();
        InputStream input = null;

        try {
            input = am.open("poma.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Serializer ser = new Persister();
        dadesXML dades = null;

        try {
            dades = ser.read(dadesXML.class, input);
        } catch (Exception e) {
            e.printStackTrace();
        }


        ArrayList<PomaXML> pomes = (ArrayList<PomaXML>) dades.getPomes();

        for(int i = 0; i<pomes.size(); i++){
            this.pomes.add(pomes.get(i));
        }

        this.mAdapter.notifyDataSetChanged();


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /*private void afegirPomes() {
        pomes.add(new Poma("poma", 1, "roig"));
        pomes.add(new Poma("poma2", 2, "roig"));
        pomes.add(new Poma("poma3", 3, "verda"));
        pomes.add(new Poma("poma4", 4, "groga"));
    }*/


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(PomaXML poma);
    }



}